#ifndef API_H_
#define API_H_

#include <iostream>
#include <cstring>
#include <vector>
#include <unistd.h>
#include "chessboard.hpp"
#include "base.hpp"

using std::string, std::vector, std::cout, std::cerr, std::cin;

// Enum for game outcome
enum class outcome_t {UNDECIDED = 0, WIN = 1, DRAW = 2};

// Macros for pipe directions
#define R 0
#define W 1

class Robochess {
    public:
    // Pipes
    int* pipe_to_rc;
    int* pipe_to_sf;

    // Moves
    vector<string> moves;
    string move_text = "position startpos moves";

    // Non-public functions
    void SendCommand(string command);
    string ReadResponse();

    // Public functions

    int movetime;

    Robochess(int* pipe_rc, int* pipe_sf);
    void IgnoreHeader();
    
    void MakeMove(string move);
    string GetMove();
    outcome_t IsGameOver(Chessboard& board);
};
#endif // API_H_
