Project to interface Stockfish with an robotic hand.


** Requirements **
** Stockfish **
Can be downloaded from the Ubuntu or Fedora repositories.
sudo dnf install stockfish (Fedora)
sudo apt install stockfish (Ubuntu)

Sometimes the Stockfish executable goes under the games folder, this program assumes is under /usr/bin. You can create a symlink or just move/copy the executable to the /usr/bin folder.

sudo ln -s /usr/games/stockfish /usr/bin/stockfish
OR
sudo mv /usr/games/stockfish /usr/bin/stockfish
OR
sudo cp /usr/games/stockfish /usr/bin/stockfish

** UART - Serial connection **
This program uses /dev/ttyS0 to communicate with the low-level Nucleo board. This requires that ttyS0 is enabled. On Raspberry Pi, it can be done by using the command "sudo raspi-config". From there choose:
Interface Options -> Serial Port -> No (Login shell) -> Yes (Serial port HW)

A reboot is recommended and you can check if it worked by checking if the file exists with "ls -l /dev/ttyS0".
