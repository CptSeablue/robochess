#ifndef KNIGHT_H_
#define KNIGHT_H_

#include "base.hpp"

class Knight : public Base {
public:
    Knight(color_n new_color, point_t new_coords) {
        name = "Knight";

        color = new_color;

        has_moved = false;
        coords = new_coords;
    }   


    piece_n type() const {
        return piece_n::KNIGHT;
    }   

    ~Knight() {
        return;
    }

    bool ValidateMove(const point_t start, const point_t end, Chessboard& board) {
        (void) (board);
		point_t move_vector = end - start;

        move_vector.x = std::abs(move_vector.x);
        move_vector.y = std::abs(move_vector.y);

        if (move_vector == point_t {2, 1} || move_vector == point_t {1, 2}) {
            return true;
        }
        return false;
    }

    void MakeMove(const point_t start, const point_t end, Chessboard& board) {
        board.table[end] = board[start];
        board.table[start] = 0;
    }
};

#endif
