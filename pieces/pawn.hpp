#ifndef PAWN_H_
#define PAWN_H_

#include "base.hpp"

class Pawn : public Base {
private:
	enum move_type_t {basic_move, long_move, en_passant};
	move_type_t move_type = basic_move;

public:
	Pawn(color_n new_color, point_t new_coords) {
		name = "Pawn";
		color = new_color;
		
		has_moved = false;
		coords = new_coords;
	}


    piece_n type() const {
        return piece_n::PAWN;
    }

	~Pawn() {
		return;
	}

    bool ValidateMove(const point_t start, const point_t end, Chessboard& board) {
		// Assume standard move
		move_type = basic_move;

		// Pawns can't move backwards so black and white pawns have different allowed signs in movement vectors
		const int invert = color == color_n::WHITE ? 1 : -1;
		
		point_t move_vector = end - start;
		move_vector.y *= invert;

		// Single forward move
		if (move_vector == point_t {0, -1}) {
			return board.table[end] == 0; // If square is free, move is allowed
		}

		// Eating move
		else if ((move_vector == point_t {1, -1}) ||
				 (move_vector == point_t {-1, -1})) {
			
			if ((board.table[end] != 0) && board.table[end]->color != color) {
				return true; // If target square is occupied by enemy piece, move is allowed.
			}

			// En passant implementation
			if (board.previous_piece != 0) { // Check for null pointer
				if (board.previous_piece->type() == piece_n::PAWN) { // If previous piece was a pawn

					const std::string prev_move = board.previous_move;
					point_t prev_start = {prev_move[0] - 'a', 7 - (prev_move[1] - '1')};
					point_t prev_end = {prev_move[2] - 'a', 7 - (prev_move[3] - '1')};
					point_t prev_vector = prev_end - prev_start;

					if (std::abs(prev_vector.y) == 2) { // If enemy pawn moved 2 squares
						if (std::abs(prev_end.x - start.x) == 1) { // If enemy pawn is adjacent
							move_type = en_passant;
							return true;
						}
					}

				}
			}

			return false;
		}

		// Double move
		else if (move_vector == point_t {0, -2}) {
			if (has_moved == 0) {
				move_type = long_move;
				return true; // If pawn hasn't moved, "double jump" is allowed.
			}

			return false;
		}

		// Unknown move
		else {
			return false;
		}
	}

    void MakeMove(const point_t start, const point_t end, Chessboard& board) {
		board.moves_since_capture = 0; // Pawn moves also count for resetting draw limit

		// Remove the en passantee pawn
		if (move_type == en_passant) {
				const std::string prev_move = board.previous_move;
				point_t prev_end = {prev_move[2] - 'a', 7 - (prev_move[3] - '1')};
				
				// Delete piece to be eaten
    			if (board.is_original == true && board[end] != 0) {
					delete board.table[prev_end];
				}
				
				board.table[prev_end] = 0;
		}

		// Move piece
		board.table[end] = board[start];
		board.table[start] = 0;

		// Promotion 
		int final_rank = (color == color_n::WHITE) ? 0 : 7;  
		if (end.y == final_rank) {
			board.PromotePiece(end);
		}
    }
};

#endif
