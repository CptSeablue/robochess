#ifndef PIECES_H_
#define PIECES_H_

#include "base.hpp"
#include "pawn.hpp"
#include "bishop.hpp"
#include "knight.hpp"
#include "queen.hpp"
#include "king.hpp"
#include "rook.hpp"

#endif
