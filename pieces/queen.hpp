#ifndef QUEEN_H_
#define QUEEN_H_

#include "base.hpp"
#include "rook.hpp"
#include "bishop.hpp"

class Queen : public Base {
public:
    Queen(color_n new_color, point_t new_coords) {
        name = "Queen";

        color = new_color;

        has_moved = false;
        coords = new_coords;
    }   


    piece_n type() const {
        return piece_n::QUEEN;
    }   

    ~Queen() {
        return;
    }

    bool ValidateMove(const point_t start, const point_t end, Chessboard& board) {
        // Queen can move as a rook or bishop, use already existing checks.
        Rook fake_rook(color, coords);
        if (fake_rook.ValidateMove(start, end, board)) {
            return true;
        }

        Bishop fake_bishop(color, coords);
        if (fake_bishop.ValidateMove(start, end, board)) {
            return true;
        }

        return false;
    }   

    void MakeMove(const point_t start, const point_t end, Chessboard& board) {
        board.table[end] = board[start];
        board.table[start] = 0;
    }
};

#endif
