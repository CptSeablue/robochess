#ifndef BASE_H_
#define BASE_H_

#include <vector>
#include <cstring>
#include "coords.hpp"
#include "grid.hpp"
#include "logger.hpp"

class Chessboard;

/*
	This is the class that all pieces inherit from
*/
enum class piece_n { ROOK = 0, KNIGHT = 1, BISHOP = 2, KING = 3, QUEEN = 4, PAWN = 5 };
enum class color_n { BLACK = 0, WHITE = 1};

class Base {
public:
    std::string name;
    
    color_n color;

    bool has_moved;
    point_t coords;

    virtual ~Base() = default;

    virtual piece_n type() const = 0;
    virtual bool ValidateMove(const point_t start, const point_t end, Chessboard& board) = 0;
    virtual void MakeMove(const point_t start, const point_t end, Chessboard& board) = 0;
};

#endif // BASE_H_
