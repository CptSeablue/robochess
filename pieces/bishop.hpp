#ifndef BISHOP_H_
#define BISHOP_H_

#include "base.hpp"

class Bishop : public Base {
public:
    Bishop(color_n new_color, point_t new_coords) {
        name = "Bishop";

        color = new_color;

        has_moved = false;
        coords = new_coords;
    }


    piece_n type() const { 
        return piece_n::BISHOP;
    }

    ~Bishop() {
        return;
    }

    bool ValidateMove(const point_t start, const point_t end, Chessboard& board) {
        (void) (board);
		point_t move_vector = end - start;

        // If target square contains friendly piece, don't eat it.
        if (board.table[end] != 0 && board.table[end]->color == color) {
            return false;
        }

        // Check if target move is perfectly diagonal, that means for every step on x-axis, a step is also done on the y-axis (ignoring direction)
        if (std::abs(move_vector.x) != std::abs(move_vector.y)) {
            return false;
        }

        // Normalize move_vector so that values are either -1 or 1. This gives us a heading which way the bishop is trying to move.
        move_vector.x = move_vector.x / std::abs(move_vector.x);
        move_vector.y = move_vector.y / std::abs(move_vector.y);

        // Check if path is clear, do so by checking every square between start and end, by incrementing with the normalized move_vector.
        point_t check_start = start + move_vector;
        point_t check_end = end; // Try removing subratction
        while(check_start != check_end) {
            if (board.table[check_start] != 0) {
                return false; // Piece is in the way of target square
            }

            check_start += move_vector;
        }

        return true;
    }

    void MakeMove(const point_t start, const point_t end, Chessboard& board) {
        board.table[end] = board.table[start];
        board.table[start] = 0;
    }
};

#endif
