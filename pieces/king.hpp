#ifndef KING_H_
#define KING_H_

#include "base.hpp"

class King : public Base {
private:    
    enum move_type_t {basic_move, castle_move};
    move_type_t move_type = basic_move;
    point_t castle_rook_start;
    point_t castle_rook_end;

public:
    King(color_n new_color, point_t new_coords) {
        name = "King";

        color = new_color;

        has_moved = false;
        coords = new_coords;
    }   


    piece_n type() const {
        return piece_n::KING;
    }   

    ~King() {
        return;
    }

    bool ValidateMove(const point_t start, const point_t end, Chessboard& board) {
		point_t move_vector = end - start;

        // Assume standard one step
        move_type = basic_move;

        // Make sure target square isn't occupied by ally piece.
        if (board[end] != 0 && board[end]->color == color) {
            return false;
        }

        // King can move 1 step in any direction, that means the absolute value of move_vector has to be less or equal than {1, 1}
        move_vector.x = std::abs(move_vector.x);
        move_vector.y = std::abs(move_vector.y);

        // Castling
        if (move_vector == point_t {2, 0}) {
            // Can't castle if king has moved
            if (has_moved == 1) return false;


            move_vector = end - start; // Undo absolute value, we need castling direction
            move_vector.x /= 2;

            point_t target_rook;
            target_rook.x = move_vector.x == 1 ? 7 : 0;
            target_rook.y = color == color_n::WHITE ? 7 : 0;

            // Check if corner piece is a rook that hasn't moved
            if ((board[target_rook] != 0) && (board[target_rook]->has_moved == 0)) {
                point_t check_square = start + move_vector;
                while(check_square != target_rook) {
                    if (board.table[check_square] != 0) {
                        return false; // Pieces are inbetween
                    }
                    check_square += move_vector;
                }

                move_type = castle_move;
                castle_rook_start = target_rook;
                castle_rook_end = end - move_vector;
                return true;
            }

            return false;
        }

        // Standard move
        if (!(move_vector <= point_t {1, 1})) {
            return false;
        }

        return true;
    }   

    void MakeMove(const point_t start, const point_t end, Chessboard& board) {
        if (move_type == castle_move) { // Move the rook aswell
            board.table[castle_rook_end] = board.table[castle_rook_start];
            board.table[castle_rook_start] = 0;
        }

        board.table[end] = board[start];
        board.table[start] = 0;

        board.kings[(int) color] = end; // Update king location in Chessboard class
    }
};

#endif
