#ifndef MAIN_H_
#define MAIN_H_

#include <iostream>
#include <array>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

#include "chessboard.hpp"
#include "api.hpp"
#include "renderer.hpp"
#include "game_logic.hpp"

// Read / Write indexes for Linux pipes
#define R 0
#define W 1

using std::string, std::cerr, std::cout, std::cin, std::endl;

#endif // MAIN_H_