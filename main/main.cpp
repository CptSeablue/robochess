#include "main.hpp"

int main() {
    // Setting up pipes
	int pipe_to_sf[2], pipe_to_rc[2];
	if (pipe(pipe_to_sf) == -1 || pipe(pipe_to_rc) == -1) {
		std::cerr << "Unable to create pipes!" << std::endl;
		return 1;
	}

    	// Create fork
	pid_t process_id;
	process_id = fork();

	// Stockfish -- child
	if (process_id == 0) {
        dup2(pipe_to_rc[W], STDOUT_FILENO);
		dup2(pipe_to_sf[R], STDIN_FILENO);

        char command[] = "/usr/bin/stockfish";
        std::array<char*, 2> args = {command, NULL};
        execv(args[0], args.data()); 

        close(pipe_to_sf[W]);
        close(pipe_to_sf[R]);
    }

	// Robochess -- parent
	else if (process_id > 0) {
        StartChess(pipe_to_sf, pipe_to_rc);

        // Clean-up fork pipes
        close(pipe_to_rc[0]);
        close(pipe_to_rc[1]);
    }

	else {
		std::cerr << "Unable to create fork!" << std::endl;
		return 1;
	}
}

