#ifndef RENDERER_H_
#define RENDERER_H_

#include <vector>
#include <cstring>
#include <base.hpp>

// Forward declaration
class Base;

void PrintTable(grid_t<Base*>& table);

namespace Chess_Textures
{

const std::vector<std::string> WHITE_ROOK = {
    R"(         )",
    R"(  |_|_|  )",
    R"(   |@|   )",
    R"(   |@|   )",
    R"(  /@@@\  )",
};

const std::vector<std::string> WHITE_KNIGHT = {
    R"(         )",
    R"(   ,,    )",
    R"(  "- \~  )",
    R"(   |@|   )",
    R"(  /@@@\  )",
};

const std::vector<std::string> WHITE_BISHOP = {
    R"(         )",
    R"(   (/)   )",
    R"(   |@|   )",
    R"(   |@|   )",
    R"(  /@@@\  )",
};

const std::vector<std::string> WHITE_QUEEN = {
    R"(   www   )",
    R"(   )@(   )",
    R"(   |@|   )",
    R"(   |@|   )",
    R"(  /@@@\  )",
};

const std::vector<std::string> WHITE_KING = {
    R"(   _+_   )",
    R"(   )@(   )",
    R"(   |@|   )",
    R"(   |@|   )",
    R"(  /@@@\  )",
};

const std::vector<std::string> WHITE_PAWN = {
    R"(    _    )",
    R"(   (@)   )",
    R"(   )@(   )",
    R"(  /@@@\  )",
    R"(         )",
};

const std::vector<std::string> BLACK_ROOK = {
    R"(         )",
    R"(  |_|_|  )",
    R"(   | |   )",
    R"(   | |   )",
    R"(  /___\  )",
};

const std::vector<std::string> BLACK_KNIGHT = {
    R"(         )",
    R"(   ,,    )",
    R"(  "- \~  )",
    R"(   | |   )",
    R"(  /___\  )",
};

const std::vector<std::string> BLACK_BISHOP = {
    R"(         )",
    R"(   (/)   )",
    R"(   | |   )",
    R"(   | |   )",
    R"(  /___\  )",
};

const std::vector<std::string> BLACK_QUEEN = {
    R"(   www   )",
    R"(   ) (   )",
    R"(   | |   )",
    R"(   | |   )",
    R"(  /___\  )",
};

const std::vector<std::string> BLACK_KING = {
    R"(   _+_   )",
    R"(   ) (   )",
    R"(   | |   )",
    R"(   | |   )",
    R"(  /___\  )",
};

const std::vector<std::string> BLACK_PAWN = {
    R"(    _    )",
    R"(   ( )   )",
    R"(   ) (   )",
    R"(  /___\  )",
    R"(         )",
};

const std::vector<std::string> A = {
    R"(         )",
    R"(   / \   )",
    R"(  /___\  )",
    R"( /     \ )",
    R"(         )",
};

const std::vector<std::string> B = {
    R"(   ___   )",
    R"(  |   \  )",
    R"(  |---<  )",
    R"(  |___/  )",
    R"(         )",
};

const std::vector<std::string> C = {
    R"(  _____  )",
    R"( |       )",
    R"( |       )",
    R"( |_____  )",
    R"(         )",
};

const std::vector<std::string> D = {
    R"(   ___   )",
    R"(  |   \  )",
    R"(  |   |  )",
    R"(  |___/  )",
    R"(         )",
};

const std::vector<std::string> E = {
    R"(   ___   )",
    R"(  |      )",
    R"(  |---   )",
    R"(  |___   )",
    R"(         )",
};

const std::vector<std::string> F = {
    R"(   ___   )",
    R"(  |      )",
    R"(  |---   )",
    R"(  |      )",
    R"(         )",
};

const std::vector<std::string> G = {
    R"(   ___   )",
    R"(  /      )",
    R"(  |  --  )",
    R"(  \___/  )",
    R"(         )",
};

const std::vector<std::string> H = {
    R"(         )",
    R"(  |   |  )",
    R"(  |---|  )",
    R"(  |   |  )",
    R"(         )",
};

const std::vector<std::string> ONE = {
    R"(         )",
    R"(   /|    )",
    R"(    |    )",
    R"(    |    )",
    R"(         )",
};

const std::vector<std::string> TWO = {
    R"(   ___   )",
    R"(  /   \  )",
    R"(      /  )",
    R"(    /    )",
    R"(  /____  )",
};

const std::vector<std::string> THREE = {
    R"(   ___   )",
    R"(      \  )",
    R"(    --|  )",
    R"(   ___/  )",
    R"(         )",
};

const std::vector<std::string> FOUR = {
    R"(         )",
    R"(  |   |  )",
    R"(  |___|  )",
    R"(      |  )",
    R"(      |  )",
};

const std::vector<std::string> FIVE = {
    R"(   ___   )",
    R"(  |      )",
    R"(  |___   )",
    R"(      \  )",
    R"(   ___/  )",
};

const std::vector<std::string> SIX = {
    R"(    __   )",
    R"(   /     )",
    R"(  /___   )",
    R"(  |   \  )",
    R"(  |___/  )",
};

const std::vector<std::string> SEVEN = {
    R"(   ___   )",
    R"(     /   )",
    R"(   -/-   )",
    R"(   /     )",
    R"(         )",
};

const std::vector<std::string> EIGHT = {
    R"(   ___   )",
    R"(  |   |  )",
    R"(  |---|  )",
    R"(  |___|  )",
    R"(         )",
};

const std::vector<std::string> BLANK = {
    R"(         )",
    R"(         )",
    R"(         )",
    R"(         )",
    R"(         )",
};

const std::vector<std::vector<std::string>> ALPHABET = {
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
};

const std::vector<std::vector<std::string>> NUMBERS = {
    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
};

const std::vector<std::vector<std::string>> TEXTURES = {
    BLACK_ROOK,
    BLACK_KNIGHT,
    BLACK_BISHOP,
    BLACK_KING,
    BLACK_QUEEN,
    BLACK_PAWN,
    WHITE_ROOK,
    WHITE_KNIGHT,
    WHITE_BISHOP,
    WHITE_KING,
    WHITE_QUEEN,
    WHITE_PAWN,
};

}
#endif
