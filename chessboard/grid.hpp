#ifndef GRID_H_
#define GRID_H_

#include <cstdint>
#include <vector>
#include <iostream>
#include "coords.hpp"

#define GRID_SIZE 8

template<typename G>
class grid_t {
public:
    std::vector<std::vector<G>> matrix;

    grid_t() {
        matrix = std::vector<std::vector<G>> (GRID_SIZE, std::vector<G> (GRID_SIZE, (G) NULL));
    }

    G& operator[] (const point_t b) {
        return matrix[b.y][b.x];
    }
};

#endif // GRID_H_
