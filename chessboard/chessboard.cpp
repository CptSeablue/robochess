#include "chessboard.hpp"

#include "pieces.hpp"
#include "grid.hpp"
#include <iostream>

Chessboard::Chessboard(void) {
    table = grid_t<Base*>();
    SetupPieces();

    kings[(int) color_n::WHITE] = {4, 7};
    kings[(int) color_n::BLACK] = {4, 0};
}

void Chessboard::CleanPieces(void) {
    // Delete all the dynamic pieces still left on the board
    for (int y = 0; y < 8; y++) {
        for (int x = 0; x < 8; x++) {
            point_t square = {x, y};

            if (table[square] != 0) {
                delete table[square];
            }
        }
    }
}

template <class PIECE>
void Chessboard::AddPiece(point_t coord, color_n color) {
    table[coord] = new PIECE(color, coord);
}

bool Chessboard::CheckIfSquareSafe(point_t square, color_n threat_color) {
    // Check for knight attacks
    const std::vector<point_t> knight_offsets = {
        { 1,  2}, {-1,  2}, {-1, -2}, { 1, -2}, 
        { 2,  1}, {-2,  1}, {-2, -1}, { 2, -1}
    };

    for (int i = 0; i < 8; i++) {
        const point_t check_square = square + knight_offsets[i];

        if (!(check_square < point_t {8, 8}) || !(check_square >= point_t {0, 0})) {
            continue;
        }

        if (table[check_square] != 0 && table[check_square]->color == threat_color) {
            if (table[check_square]->type() == piece_n::KNIGHT) {
                return false;
            }
        }
    }

    // Check horisontal and vertical lines (queen, rook, king)
    const std::vector<point_t> axis_offsets = {
        {1, 0}, {0, -1}, {-1, 0}, {0, 1}
    };

    for (int i = 0; i < 4; i++) {
        point_t check_square = square + axis_offsets[i];

        for (;; check_square += axis_offsets[i]) {
            if ( !(check_square < point_t {8, 8} && check_square >= point_t {0, 0}) ) {
                break;
            }
            
            if (table[check_square] == 0) {
                continue;
            }

            if (table[check_square]->color == threat_color) {
                switch(table[check_square]->type()) {
                    case piece_n::QUEEN:
                    case piece_n::ROOK:
                        return false;

                    case piece_n::KING: // King is only a threat when directly next to it.
                        if (square + axis_offsets[i] == check_square) return false;
                        break;
                
                    default:
                        break;
                }
            }

            break;
        }
    }

    // Check diagonal lines (queen, bishop, king, pawns). Element order is important!
    const std::vector<point_t> diagonal_offsets = {
      {-1, 1}, {1, 1}, {1, -1}, {-1, -1}  
    };

    for (int i = 0; i < 4; i++) {
        point_t check_square = square + diagonal_offsets[i];

        for (;; check_square += diagonal_offsets[i]) {
            if ( !(check_square < point_t {8, 8} && check_square >= point_t {0, 0}) ) {
                break;

            }

            if (table[check_square] == 0) {
                continue;
            }

            if (table[check_square]->color == threat_color) {
                switch(table[check_square]->type()) {
                    case piece_n::QUEEN:
                    case piece_n::BISHOP:
                        return false;

                    case piece_n::KING: // King is only a threat when directly next to it.
                        if (square + diagonal_offsets[i] == check_square) return false;
                        break;    

                    case piece_n::PAWN: // Pawn is only a threat when directly next to it on one side.
                        if (square + diagonal_offsets[i] != check_square) break;
                        if (threat_color == color_n::WHITE && i < 2) return false;
                        if (threat_color == color_n::BLACK && i > 1) return false;
                        break;
                
                    default:
                        break;
                }
            }

            break;
        }
    }

    return true;
}

color_n Chessboard::LastMoveColor(void) {
    point_t end = {previous_move[2] - 'a', 7 - (previous_move[3] - '1')};
    return table[end]->color;
}

bool Chessboard::OnlyKingsRemain(void) {
    for (int y = 0; y < 8; y++) {
        for (int x = 0; x < 8; x++) {
            point_t square = {x, y};
            if (table[square] != 0 && table[square]->type() != piece_n::KING) {
                return false;
            }
        }
    }

    return true;
}

bool Chessboard::IsThreefoldRepetition(void) {
    if (move_count < 8) return false;

    for (int i = 0; i < 4; i++) {
        if (previous_eight_moves[i].compare(previous_eight_moves[i + 4]) != 0) {
            return false;
        }
    }
    return true;
}

void Chessboard::MakeMove(std::string move) {
    point_t start = {move[0] - 'a', 7 - (move[1] - '1')};
    point_t end = {move[2] - 'a', 7 - (move[3] - '1')};

    // Delete piece to be eaten
    if (is_original == true && table[end] != 0) {
        moves_since_capture = 0;
        delete table[end];
    }

    moves_since_capture++;
    table[start]->MakeMove(start, end, *this);
    table[end]->has_moved = true;

    previous_move = move;
    previous_piece = table[end];

    // Add move to history
    move_count++;
    for (int i = 0; i < 7; i++) {
        previous_eight_moves[7 - i] = previous_eight_moves[7 - i - 1];
    }
    previous_eight_moves[0] = move;
}

bool Chessboard::ValidateMove(std::string move) {
    // Normal moves are 4 characters, promoting moves 5
    if (move.size() < 4) {
        return false;
    }

    next_promotion = 'q';
    if (move.size() == 5) {
        // Inelegant but fast?! way to check if value in array
        switch (move[4]) {
            case 'q':
            case 'n':
            case 'r':
            case 'b':
                next_promotion = move[4];
                break;
        
            default:
                return false;
        }
    }

    point_t start = {move[0] - 'a', 7 - (move[1] - '1')};
    point_t end = {move[2] - 'a', 7 - (move[3] - '1')};

    // Start or end squares are outside the board
    if (!(start <= point_t {8, 8}) || !(start <= point_t {8, 8})) {
        return false;
    }

    // No target at board
    if (table[start] == 0) {
        return false;
    }
    // Check if move is even valid
    if (!table[start]->ValidateMove(start, end, *this)) {
        return false;
    }

    // Simulate the move to check if king would fall to danger
    Chessboard simulated_board = *this;
    simulated_board.is_original = false;
    simulated_board.MakeMove(move);

    point_t king_square = simulated_board.kings[(int) simulated_board[end]->color];
    color_n enemy_color = (simulated_board[end]->color == color_n::WHITE) ? color_n::BLACK : color_n::WHITE;
   
    if (!simulated_board.CheckIfSquareSafe(king_square, enemy_color)) {
        return false;
    }

    return true;
}

void Chessboard::SetupPieces(void) {
    #if 1
    // Add the pawns
    for (uint8_t x = 0; x < 8; x++) {
        AddPiece<Pawn> ({x, 6}, color_n::WHITE);
        AddPiece<Pawn> ({x, 1}, color_n::BLACK);
    }

    // Add the rooks
    AddPiece<Rook> ({0, 0}, color_n::BLACK);
    AddPiece<Rook> ({7, 0}, color_n::BLACK);
    AddPiece<Rook> ({0, 7}, color_n::WHITE);
    AddPiece<Rook> ({7, 7}, color_n::WHITE);

    // Add the knights
    AddPiece<Knight> ({1, 0}, color_n::BLACK);
    AddPiece<Knight> ({6, 0}, color_n::BLACK);
    AddPiece<Knight> ({1, 7}, color_n::WHITE);
    AddPiece<Knight> ({6, 7}, color_n::WHITE);
    
    // Add the bishops
    AddPiece<Bishop> ({2, 0}, color_n::BLACK);
    AddPiece<Bishop> ({5, 0}, color_n::BLACK);
    AddPiece<Bishop> ({2, 7}, color_n::WHITE);
    AddPiece<Bishop> ({5, 7}, color_n::WHITE);

    // Add the king and queen
    AddPiece<Queen> ({3, 0}, color_n::BLACK);
    AddPiece<King>  ({4, 0}, color_n::BLACK);
    
    AddPiece<Queen> ({3, 7}, color_n::WHITE);
    AddPiece<King>  ({4, 7}, color_n::WHITE);
    #endif
}

void Chessboard::PromotePiece(point_t coord) {
    if (is_original == false) {
        return;
    }

    color_n piece_color = table[coord]->color;
    delete table[coord];

    switch (next_promotion) {
        case 'r':
        AddPiece<Rook>(coord, piece_color);
        break;

        case 'b':
        AddPiece<Bishop>(coord, piece_color);
        break;

        case 'n':
        AddPiece<Knight>(coord, piece_color);
        break;

        default:
        AddPiece<Queen>(coord, piece_color);
        break;
    }
}